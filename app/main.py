import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from joblib import dump

# Load the Iris dataset
iris_df = pd.read_csv('https://raw.githubusercontent.com/uiuc-cse/data-fa14/gh-pages/data/iris.csv')

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(
    iris_df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']],
    iris_df['species'], 
    test_size=0.2,
    random_state=42
)

# Fit a logistic regression model to the training data
lr = LogisticRegression(max_iter=1000)
lr.fit(X_train, y_train)

# Save the trained model to disk
dump(lr, 'iris_classifier.joblib')
